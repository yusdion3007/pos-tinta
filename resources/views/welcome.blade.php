<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Dashboard</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  @include('components.sidebar')

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        @include('components.topbar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Penjualan</h1>
          </div>

          <div class="row">
          
            <div class="card shadow mb-4 col-md-9">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List Items</h6>
              </div>
              <div class="card-body">
                          <!-- Content Row -->
                <div class="row">
                  <!-- Earnings (Monthly) Card Example -->
                  @foreach($data['product'] as $product)
                  <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                      <div class="card-body">
                        <img src="http://venetaonline.com/uploads/product/CISS_EPSON_L800_Poto_Cyan2.jpg" width="80" height="80">
                        <div class="row no-gutters align-items-center">
                          <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 name_product_class">{{ $product->product_name }}</div>
                            <div class="text-xs font-weight-bold text-gray-500 text-uppercase mb-1">Stock {{ $product->stock }}</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Rp.{{ number_format($product->price) }}</div>
                          </div>
                          <div class="col mr-2">
                            <div class="quantity">
                              <a href="#" class="quantity__minus" id="quantity__minus{{ $product->no }}"><span>-</span></a>
                              <input name="quantity" type="text" class="quantity__input" id="quantity__input{{ $product->no }}" value="0">

                              <input type="hidden" id="product_pricing{{ $product->no }}" value="{{ $product->price }}"/>
                              <input type="hidden" id="name_product{{ $product->no }}" value="{{ $product->product_name }}"/>
                              <input type="hidden" id="product_id_store{{ $product->no }}" value="{{ $product->id }}"/>

                              <a href="#" class="quantity__plus" id="quantity__plus{{ $product->no }}"><span>+</span></a>
                            </div>
                          </div>
                          <div class="col-auto">
                            <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
              </div>
            </div>
            </div>

              <div class="col-md-3">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Struk Record</h6> 
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <div class="form-group">
                       <label for="exampleInputEmail1">No. Order (Invoice)</label>
                       <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="#: INV-78225">
                    </div>
                    <hr>
                    <div class="list_invoice">
                      
                    </div>
                    <hr>
                  </div>
                </div>

                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Record</h6> 
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                      <label for="exampleInputEmail1">Subtotal</label>
                      <label for="exampleInputEmail1"><p id="price_product"></p></label>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between">
                      <label for="exampleInputEmail1">PPN 10%</label>
                      <label for="exampleInputEmail1"><p id="ppn_10"></p></label>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between">
                      <label for="exampleInputEmail1">Total</label>
                      <label for="exampleInputEmail1"><p id="total_price"></p></label>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between">
                      <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-flag"></i>
                        </span>
                        <span class="text">Lanjut</span>
                      </a>
                      <a href="#" class="btn btn-danger btn-icon-split">
                        <span class="icon text-white-50">
                          <i class="fas fa-flag"></i>
                        </span>
                        <span class="text">Skip</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pilih Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form method="post" action="{{url('add-transaction')}}" enctype="multipart/form-data" class="mt-2">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">List Customer</label>
                          <select class="form-control" id="select-customer" name="customer_id" style="width: 100%" id="js-example-tags">
                            <option value="0" selected="selected">Data Baru</option>
                            @foreach($data['customer'] as $customer)
                              <option value="{{ $customer->id }}">{{ $customer->customer_name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group data_cus_form">
                          <label for="message-text" class="col-form-label">Nama Pelanggan:</label>
                          <input class="form-control" id="customer_name" name="customer_name" required/>
                        </div>
                        <div class="form-group data_cus_form">
                          <label for="message-text" class="col-form-label">Nama Perusahaan:</label>
                          <input class="form-control" id="company" name="company" required/>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group data_cus_form">
                              <label for="message-text" class="col-form-label">Telephone:</label>
                              <input class="form-control" id="telp" name="telp" required/>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group data_cus_form">
                              <label for="message-text" class="col-form-label">Total Piutang Sebelumnya :</label>
                              <input class="form-control" id="piutang" name="piutang" readonly required/>
                            </div>
                          </div>
                        </div>
                        <div class="form-group data_cus_form">
                          <label for="message-text" class="col-form-label">Alamat:</label>
                          <input class="form-control" id="address" name="address" required/>
                        </div>
                        <div class="form-group data_cus_form">
                          <label for="message-text" class="col-form-label">Jatuh Tempo :</label>
                          <input type="date" class="form-control" id="time_of_payment" name="time_of_payment" required/>
                        </div>

                        <input type="hidden" id="product_id" name="product_id[]" />
                        <input type="hidden" id="product_qty" name="product_qty[]" />
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Tinta 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="js/custom.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

  <script>
    $("#js-example-tags").select2({
      tags: true
    });
  </script>

  <script>
    $(document).ready(function() {
      function addCommas(nStr) {
          nStr += '';
          var x = nStr.split('.');
          var x1 = x[0];
          var x2 = x.length > 1 ? '.' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
              x1 = x1.replace(rgx, '$1' + ',' + '$2');
          }
          return x1 + x2;
      }
      
      // var base_url = "http://localhost/pos-tinta/public";
      var base_url = "http://pos.chopper-tech.com";

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });	

      $("#select-customer").click(function(e){
          e.preventDefault();
          var customer_id = $("#select-customer").val();
          console.log("Select Customer " + customer_id);
          var string = "";
          if(customer_id != 0){
            $.ajax({
                type:'GET',
                url: base_url+'/check-customer/'+customer_id,
                success:function(data){
                    if(data.code == 501){
                      console.log("ada dong :" + data.data.piutang);
                      $("#customer_name").val(data.data.customer_name);
                      $("#company").val(data.data.company);
                      $("#telp").val(data.data.telp);
                      $("#address").val(data.data.address);
                      $("#piutang").val("Rp. " + addCommas(data.data.piutang));
                    } else {
                      console.log("nggak ada dong :" + customer_id);
                      $("#customer_name").val(string);
                      $("#company").val(string);
                      $("#telp").val(string);
                      $("#address").val(string);
                      $("#piutang").val(string);
                    }
                }
            });
          } else {
            console.log("Data Baru :" + customer_id);
            $("#customer_name").val(string);
            $("#company").val(string);
            $("#telp").val(string);
            $("#address").val(string);
          }
      });
      
      var minus = [];
      var plus = [];
      var input = [];
      $(".quantity__minus").each(function(){
        minus.push($(this).attr('id'));
      });
      $(".quantity__plus").each(function(){
        plus.push($(this).attr('id'));
      });
      $(".quantity__input").each(function(){
        input.push($(this).attr('id'));
      });

      $.each(minus, function(){
        var urutan = this.substr(this.length - 1);        
        $("#"+this).click(function(e) {
          e.preventDefault();
          var value = $("#quantity__input"+urutan).val();
          if (value > 0) {
            value--;
          }
          $("#quantity__input"+urutan).val(value);
          getQty();
        });
      });
      
      $.each(plus, function(){
        var urutan = this.substr(this.length - 1);
        $("#"+this).click(function(e) {
          e.preventDefault();
          var value = $("#quantity__input"+urutan).val();
          value++;
          $("#quantity__input"+urutan).val(value);
          getQty();
        })
      });
      
      $.each(input, function(){
        var urutan = this.substr(this.length - 1);
        $("#"+this).change(function(e) {
          e.preventDefault();
          var value = $("#quantity__input"+urutan).val();
          $("#quantity__input"+urutan).val(value);
          getQty();
        })
      });

      function getQty(newdiv){
        var count_product = <?php echo $data['count']; ?>;
        var list_invoice = [];
        var product_id_temp = [];
        var qty_temp = [];
        var total = 0;
        for(i = 1; i <= count_product; i++ ){
          var qty = $("#quantity__input"+i).val();
          if(qty > 0){
            var price = $("#product_pricing"+i).val();
            var product_total = (qty * price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var newdiv = $( `<div class='d-flex justify-content-between'><label for='exampleInputEmail1'>${$('#name_product'+i).val()}</label><label for='exampleInputEmail1'>${qty}</label><label for='exampleInputEmail1'>${product_total}</label></div>` );

            list_invoice.push(newdiv);
            product_id_temp.push($("#product_id_store"+i).val());
            qty_temp.push(qty);
            total += qty * price;
          }
        }
        $('.list_invoice').html( list_invoice );


        $('#product_id').val( product_id_temp );
        $('#product_qty').val( qty_temp );

        $('#price_product').html("Rp. " + total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#ppn_10').html(((total * 10) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#total_price').html("Rp. " + (total + (total * 10) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

          // var tot = 0;
          // $(".quantity__input").each(function() {
          //   tot += Number($(this).val());
          // });
          // $('#product_name').html($('#name_product').html());
          // $('#qty_product').html(tot);
          // $('#price_product_piece').html(tot * 10000);
          // $('#price_product').html(tot * 10000)
          // $('#ppn_10').html((tot * 10000) * 10 / 100)
          // $('#total_price').html(tot * 10000 + (tot * 10000) * 10 / 100)
          
          // var $newdiv = $( `<div class='d-flex justify-content-between'><label for='exampleInputEmail1'>${$('#name_product').html()}</label><label for='exampleInputEmail1'>${tot}</label><label for='exampleInputEmail1'>${tot * 10000}</label></div>` );
          // var $newdiv2 = $( `<div class='d-flex justify-content-between'><label for='exampleInputEmail1'>${$('#name_product').html()}</label><label for='exampleInputEmail1'>${tot}</label><label for='exampleInputEmail1'>${tot * 10000}</label></div>` );
          // $('.list_invoice').html( [ $newdiv, $newdiv2 ] );
        }

    });
  </script>
</body>

</html>
<ul class="nav navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">POS Printer</div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Penjualan</span></a>
      </li>
      @if(Auth::user()->role == 2)
      <div style="display:none">
      @else
      <div>
      @endif
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          POS Management
        </div>
        <li class="nav-item">
          <a class="nav-link" href="{{url('user-management')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>User Management</span></a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="{{url('branch-management')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Branch Management</span></a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link" href="{{url('category-management')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Category Management</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/item-management')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Items Management</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/customer-management')}}">
            <i class="fas fa-fw fa-table"></i>
            <span>Customer Management</span></a>
        </li>
        <li class="nav-item start active open">
          <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fas fa-fw fa-table"></i>
              <span>Order</span>
              <span class="arrow open"></span>
          </a>
          <ul class="sub-menu">
                <li class="nav-item">
                    <a href="{{ url('order-list') }}" class="nav-link">
                        <i class="icon-layers"></i>
                        <span class="title">Order List</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('order-retur') }}" class="nav-link">
                        <i class="icon-layers"></i>
                        <span class="title">Order Retur</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('report-management')}}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Report Management</span></a>
        </li>
        <div class="sidebar-heading">
          Cash
        </div>
        <li class="nav-item">
          <a class="nav-link" href="{{url('payment')}}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Penerimaan Cash</span></a>
        </li>
      </div>
      <hr class="sidebar-divider d-none d-md-block">
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
      <hr class="sidebar-divider">
    </ul>